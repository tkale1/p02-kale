//
//  AppDelegate.h
//  p02-kale
//
//  Created by Tanmay Kale on 2/2/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

