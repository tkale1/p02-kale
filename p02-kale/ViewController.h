//
//  ViewController.h
//  p02-kale
//
//  Created by Tanmay Kale on 2/2/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *blocks;

@property(strong,nonatomic) UIButton *up;
@property (strong, nonatomic) UIButton *down;
@property (strong, nonatomic) IBOutlet UIButton *left;
@property (strong, nonatomic) IBOutlet UIButton *right;
@property (strong, nonatomic) IBOutlet UILabel *currentScore;
@property (strong, nonatomic) IBOutlet UILabel *highestScore;

@property (strong, nonatomic) IBOutlet UIButton *resetButton;
extern int globalScore;
@property (strong, nonatomic) IBOutlet UILabel *gameOver;

//extern int highScore;
-(void)generateRand;
@end

