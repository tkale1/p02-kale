//
//  ViewController.m
//  p02-kale
//
//  Created by Tanmay Kale on 2/2/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize blocks;
@synthesize resetButton;
@synthesize up, down,right,left;
int globalScore=0;

NSUserDefaults *defaults;



/*
        The Rand function is used to generate random numbers either 2 or 4.
        4 is generated inorder to increase the chances of winning the game.
 */
-(void)Rand{
    NSInteger num = arc4random_uniform(16);
    if([[[blocks objectAtIndex:num] text] isEqual:@""])
    {
        [self colorCheck];
        [[blocks objectAtIndex:num] setText: @"2"];
    }
    
    // For generating second Random Number
    NSInteger num1 = arc4random_uniform(16);
    if(num1==num)           // to check if same random number is generated for the 2nd block as well.
    {
        num1 = arc4random_uniform(16);
    }
    if([[[blocks objectAtIndex:num1] text] isEqual:@""])
    {
        [self colorCheck];
        [[blocks objectAtIndex:num1] setText: @"2"];
    }
    
}

/*
        The generateRand function is used to generate random numbers from 1-16.
        4 is generated inorder to increase the chances of winning the game.
 */

-(void)generateRand{
    NSInteger num1 = arc4random_uniform(16);
    
    if(!([[[blocks objectAtIndex:num1] text] isEqual:@""]))           // to check if random number  generated is a block number which is empty.
    {
        num1 = arc4random_uniform(16);
    }
    if([[[blocks objectAtIndex:num1] text] isEqual:@""])
    {
        NSInteger rand4= arc4random_uniform(20);
        if(rand4>13)
        {
            [self colorCheck];
            [[blocks objectAtIndex:num1] setText: @"4"];
        }
        else
        {
            [self colorCheck];
            [[blocks objectAtIndex:num1] setText: @"2"];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
       
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    NSInteger theHighScore = [defaults integerForKey:@"HighScore"];
    _highestScore.text= [NSString stringWithFormat:@"%ld", (long)theHighScore];
    
    [self Rand];
    
}
//Code to change color of the label on clubbing the nos.
-(void)colorCheck{
    for(int i=0;i<=15;i++)
    {
        if(([[[blocks objectAtIndex:i] text] isEqual:@"2"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor colorWithRed:81.0f/255.0f green:126.0f/255.0f blue:176.0f/255.0f alpha:1.0f]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"4"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor colorWithRed:225.0f/255.0f green:97.0f/255.0f blue:176.0f/255.0f alpha:1.0f]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"8"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor whiteColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"16"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor colorWithRed:0.0f/255.0f green:82.0f/255.0f blue:214.0f/255.0f alpha:1.0f]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"32"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor yellowColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"64"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor grayColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"128"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor colorWithRed:0.0f/255.0f green:161.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"256"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor brownColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"512"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor purpleColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"1024"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor darkGrayColor]];
        }
        if(([[[blocks objectAtIndex:i] text] isEqual:@"2048"]))
        {
            [[blocks objectAtIndex:i] setTextColor:[UIColor whiteColor]];
        }
    }
}

//Check if current score is greater than the Highscore. if yes the set new highscore
-(void)checkHighScore{
    NSInteger theHighScore = [defaults integerForKey:@"HighScore"];
    if(globalScore>theHighScore)
    {
        [defaults setInteger:globalScore forKey:@"HighScore"];
        _highestScore.text= [NSString stringWithFormat:@"%ld", (long)globalScore];
    }
    
}

// TO check if there are any possible moves left in the game.
-(void)checkGameOver{
    int count = 0;
    Boolean c_flag = false;;
    //checking for gameOver
    for (int go=0; go<=11; go++)
    {
        if([[[blocks objectAtIndex:go] text] isEqual:@"2048"])
        {
            _gameOver.text= @"You Won the Game...!!! :) :)";
            _gameOver.hidden=NO;
            down.enabled=NO;
            up.enabled=NO;
            right.enabled=NO;
            left.enabled=NO;
            
        }
        count++;
        if([[[blocks objectAtIndex:go] text] isEqual:@""])
        {
            c_flag = true;
        }
        else
        {
            if((count%4) == 0)
            {
                if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+4)] text]])
                {
                    c_flag = true;
                }
            }
            else
            {
                if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+1)] text]])
                {
                    c_flag = true;
                }
                else if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+4)] text]])
                {
                    c_flag = true;
                }
            }
            
            if(go == 11)
            {
                if([[[blocks objectAtIndex:12] text] isEqualToString:[[blocks objectAtIndex:13] text]])
                {
                    c_flag = true;
                }
                else if([[[blocks objectAtIndex:13] text] isEqualToString:[[blocks objectAtIndex:14] text]])
                {
                    c_flag = true;
                }
                else if([[[blocks objectAtIndex:14] text] isEqualToString:[[blocks objectAtIndex:15] text]])
                {
                    c_flag = true;
                }
            }
            
        }
        
        if(c_flag)
            break;
    }
    if(c_flag==false)
    {
        _gameOver.hidden=NO;
        down.enabled=NO;
        up.enabled=NO;
        right.enabled=NO;
        left.enabled=NO;
    }
    else
    {
        [self generateRand];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)up:(id)sender {
    for (int j=0; j<=3; j++) {
        //Boolean flag = false;
        //this is for location 0
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
            [[blocks objectAtIndex:(j+4)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
        }
    }
    
    //add
    int k = 0;
    while(k<=11)
    {
        if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+4)] text]]) {
            
            
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k+4)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+4)] setText:@""];
            }
        }
        k++;
        _currentScore.text= [NSString stringWithFormat:@"%d", globalScore];
        [self checkHighScore];
        
    }
    for (int j=0; j<=3; j++) {
        
        //this is for location 0
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
            [[blocks objectAtIndex:(j+4)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
                
            }
            else
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
        }
    }
    
    [self checkGameOver];
}

- (IBAction)down:(id)sender {
    for(int dwn = 0;dwn < 4 ; dwn++)
    {
        //chking last row of columns
        if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""])
        {
            [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
            [[blocks objectAtIndex:(dwn+8)] setText:@""];
        }
        
        //checking 2nd last row of columns
        if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
            }
            else
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
            }
        }
        //checking 2nd row of column
        if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
                
            }
            else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
                
            }
            else
            {
                [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
                
                
            }
        }
    }
    
    
    //addition logic
    int k = 15;
    while(k>=4)
    {
        if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k-4)] text]])
        {
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k-4)] text] intValue];
            int res = m + n;
            
            if(res > 0)
            {
                globalScore+=res;
                
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k-4)] setText:@""];
                
            }
        }
        k--;
        _currentScore.text= [NSString stringWithFormat:@"%d", globalScore];
        [self checkHighScore];
    }
    
    
    //making blocks go down
    for(int dwn = 0;dwn < 4 ; dwn++)
    {
        //chking last row of columns
        if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
            [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
            [[blocks objectAtIndex:(dwn+8)] setText:@""];
            
        }
        //checking 2nd last row of columns
        if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
                
            }
            else
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
                
            }
        }
        //checking 2nd row of column
        if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            else
            {
                [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
                
            }
        }
    }
    
    [self checkGameOver];
}

- (IBAction)left:(id)sender {
    int j=0;
    while(j<=12) {
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
            [[blocks objectAtIndex:(j+1)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
                
            }
            else
            {
                
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
        }
        j=j+4;
    }
    
    //add
    int k = 0;
    while(k<=12)
    {
        if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+1)] setText:@""];
            }
        }
        if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
            int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+2)] setText:@""];
            }
        }
        if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+3)] text]]) {
            int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+3)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+3)] setText:@""];
            }
        }
        k=k+4;
        _currentScore.text= [NSString stringWithFormat:@"%d", globalScore];
        [self checkHighScore];
    }
    
    
    j=0;
    while(j<=12) {
        
        //this is for location 0
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
            [[blocks objectAtIndex:(j+1)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
                
            }
            else
            {
                
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
        }
        j = j+4;
    }
    
    //random generation after checking all the blocks
    // [self generateRand];
    //[self colorCheck];
    [self checkGameOver];
    
}

- (IBAction)right:(id)sender {
    int j=0;
    while(j<=12) {
        if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
        {
            [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+2)] text]];
            [[blocks objectAtIndex:(j+2)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:(j+2)] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
                
            }
            else
            {
                
                [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:(j+1)] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:(j+1)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
        }
        j=j+4;
    }
    
    //add
    int k = 0;
    while(k<=12)
    {
        if ([[[blocks objectAtIndex:(k+3)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
            int m = [[[blocks objectAtIndex:k+3] text] intValue];
            int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:k+3] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+2)] setText:@""];
            }
        }
        if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
            int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+1)] setText:@""];
            }
        }
        if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k)] text]]) {
            int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int n = [[[blocks objectAtIndex:(k)] text] intValue];
            int res = m + n;
            if(res > 0)
            {
                globalScore+=res;
                [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",res] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k)] setText:@""];
            }
        }
        k=k+4;
        _currentScore.text= [NSString stringWithFormat:@"%d", globalScore];
        [self checkHighScore];
    }
    
    
    j=0;
    while(j<=12) {
        
        //this is for location 0
        if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+2)] text]];
            [[blocks objectAtIndex:(j+2)] setText: @""];
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
                
            }
            else
            {
                
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
        }
        j = j+4;
    }
    
    //random generation after checking all the blocks
    // [self generateRand];
    //[self colorCheck];
    [self checkGameOver];
}
- (IBAction)resetScore:(id)sender {
    for(int i=0;i<=15;i++)
    {
        [[blocks objectAtIndex:i] setText:@""];
    }
    _currentScore.text= @"0";
    [self Rand];
    _gameOver.hidden=YES;
}
@end
